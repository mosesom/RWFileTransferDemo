# RWFileTransferDemo

本Demo主要是把公司项目中用到的文件传输技术整理出来，其中使用到iOS 7.0 后推出的Mutipeer Connectivity框架，借助WIFI、蓝牙来建立传输通道，达到用户互相传输媒体的功能

![1](https://gitee.com/uploads/images/2018/0518/170750_a3afd0f4_119514.jpeg "1.JPG")
![2](https://gitee.com/uploads/images/2018/0518/170758_de6d0bbe_119514.jpeg "2.JPG")
![3](https://gitee.com/uploads/images/2018/0518/170805_5853da08_119514.jpeg "3.JPG")
![4](https://gitee.com/uploads/images/2018/0518/170553_e898f368_119514.png "44.png")